License: BSD
Dependencies

    python (2.6+)
    tornado

Besides the name and the version of the package being distributed, the most important arguments setup can receive are:

    description: HTTP SERVER DEDICATED TO STATS
    long_description: HTTP SERVER DEDICATED TO STATS
    keywords: STATS, HTTP, SERVER
    author: Laureote Loïc
    author_email: laureote-loic@hotmail.fr
    url: http://www.tornadoweb.org/
    license: The license (GPL, LGPL, and so on)
    packages: PYTHON-TORNADO
    namespace_packages: A list of namespaced packages

Windows :

	Download and install python27
	Download and install numpy-1.7.1-win32-superpack-python2.7.exe
	Download tornado


Tornado Installation :

Installation :

Automatic installation:

	pip install tornado
	Tornado is listed in PyPI and can be installed with pip or easy_install. 
	Note that the source distribution includes demo applications that are not present when Tornado is installed in this way,
	so you may wish to download a copy of the source tarball as well.

Manual installation: Download the latest source from PyPI.

	tar xvzf tornado-$VERSION.tar.gz
	cd tornado-$VERSION
	python setup.py build
	sudo python setup.py install

The Tornado source code is hosted on GitHub.

	Prerequisites: Tornado runs on Python 2.6, 2.7, 3.2, and 3.3
	It has no strict dependencies outside the Python standard library, 
	although some features may require one of the following libraries:


Download leela from github
Install a text editor like geany
go with command lines in leela directoy and run leela by c:\python27\python.exe start_app.py 
