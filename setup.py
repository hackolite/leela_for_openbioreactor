import os
from setuptools import setup, find_packages
version = '0.1.0'
README = os.path.join(os.path.dirname(__file__), 'README.txt')
long_description = open(README).read() + 'nn'
setup(name='Calculation',
      version=version,
      description=("A package that deals with STAT, "
                    "from TOTO inc"),
      long_description=long_description,
      keywords='LEeLA',
      author='ToTo',
      author_email='laureote-loic@hotmail.fr',
      license='GPL',
      packages=find_packages(),
      namespace_packages=['Calculation','Template','Graph_Class','Sax'],
      install_requires=['python-tornado']
      )
