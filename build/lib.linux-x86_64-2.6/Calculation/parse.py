import numpy
from formule_list import *

#cette classe permet de parser la formule et recupere les listes traitees par calcule 



class Parse_Graph :
			
		
		
		#data.csv = stockage des datas dans variable	
				
		def __init__(self):
			self.o = Formulist()
			self.data = self.o.read_csv('data.csv')			
			self.list_lists = []
				
		
		
		#une fonction pour verifier que la liste des operations est correctement recupere
		def get_desc_lists(self,string):			
			desc_list = []			
			list_symbol = {'(':['cheese',')'],'[':['bar',']'],'{':['line','}']}			
			
			
			
			plenty_graph = string.split(' ')
			
			for i in plenty_graph :
				for sy in list_symbol.keys():						
					if i.count(sy) > 0 :
						tmp = []
						i = i.replace(sy,'')
						i = i.replace(list_symbol[sy][1],'')
						calc = i.split(',')
						for particule in calc:						
							tmp.append(list(self.o.calculation(self.data,particule)))
						desc_list.append([list_symbol[sy][0],tmp])
				
			return desc_list
				
				
if __name__ == '__main__':	
	exemple = Parse_Graph()

