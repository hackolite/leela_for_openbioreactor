#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.


from datetime import *
import time
from matplotlib.font_manager import FontProperties
import matplotlib.pyplot as plt
import getopt
import sys 
from optparse import OptionParser



class DataFactory(object):
    
    """data factory"""
	
    def __init__(self, datatype=None):      
		"""Data factory is our abstract factory"""
		self.size = 0
		self.datatype=datatype
		self.interv = {'month':86400,'day':3600}
    
    def show_data_type(self):
		print self.datatype


class Db_slurm_event(DataFactory):
	"""database factory"""
	
	def __init__(self):
		self.name = "my_name_is_db_datatype"
		self.datatype='database_slurm'
		DataFactory.__init__(self)
	
	#extract des donnees de base
	def database_connect(user,passwd,dbname) :	
		conn = MySQLdb.connect(host="batch", user="slurmro", db="slurm_acct_db")
		cur = conn.cursor()	
		cur.execute("SELECT * from event_slurm_database")
		row = cur.fetchone()
		return result
	
	#design pattern factory
	#calcul statistique	
	
	def extract_event(self,start,end,intervalle,output):
		
		deltatime = self.interv[intervalle]
		start_ref = int(start)
		end_ref = int(end)
		result_ref = []
		dict_result={}
		dict_days_result={}
		
		mavend=0
		
		listing_output = output.readlines()
		count=0

		#formatage du fichier output.txt
		for i in listing_output:
			start = int(i.split(',')[0].replace('(','').replace('L',''))
			end = int(i.split(',')[1].replace('L',''))
			#conversion des 0 en today-time
			if int(end) == 0:
				end = time.time()
			#print 'test',i.split(',')[2],start,end
			if start_ref<start<end_ref or start_ref<end<end_ref:
				count = count +1
				#print i.split(',')[5]
				#print start,end
				dict_result[count]=[i.split(',')[2],start,end,'val',i.split(',')[5]]


		#12
		bm1= ['bm01','bm02','bm03','bm04','bm05','bm06','bm07','bm08','bm09','bm10','bm11','bm12','bm13','bm14','bm15','bm16']

		#32
		bm2= ['bm16','bm17','bm18','bm19','bm','bm06','bm07','bm08','bm09','bm10','bm11','bm12','bm13','bm14','bm15','bm16']
		#cg
	
		cpucount_list=[]
		cpucount = 0
		time_list=[]
		count_sign_reason=[]
		mylist = range(start_ref,end_ref,deltatime)

		for sec in mylist:
			liste_reason = []
			#print sec
			node=0
			cpucount=0
			liste_nodes=[]
			for dat in dict_result:

				if dict_result[dat][1]<sec<dict_result[dat][2] or dict_result[dat][1]<sec<dict_result[dat][2]:
					node = node+1
					liste_nodes.append(dict_result[dat][0])
					liste_reason.append(dict_result[dat][-1])
			t= datetime.fromtimestamp(sec)
			sec = t.strftime('%Y-%m-%d-%H-%M-%S')

			print liste_nodes 
			
			for no in liste_nodes:
				if no in bm2:
					cpucount = cpucount+32
				else :
					cpucount = cpucount+12
	
				

			for i in list(set(liste_reason)):
				if int(liste_reason.count(i))> 50:
					#pass
					plt.text(t,cpucount,i)

			time_list.append(t)
			cpucount_list.append(cpucount)

			
		
		plt.plot(time_list,cpucount_list)
		plt.ylabel('off-cpu-number-on-ivanoe')
		plt.xlabel(str(datetime.fromtimestamp(start_ref).strftime('%Y-%m-%d-%H-%M-%S'))+'/'+str(datetime.fromtimestamp(end_ref).strftime('%Y-%m-%d-%H-%M-%S')))
		plt.grid(True)
		plt.show()


class Data_trac(DataFactory):
	"""trac factory"""
	def __init__(self):
		"""DataTracfactory"""
		self.name = "my_name_is_TracDataType"



class Db_slurm_jobstat(DataFactory):
	"""trac factory"""
	def __init__(self):
		"""DataTracfactory"""
		self.name = "my_name_is_Slurm_Jobstat"

def usage():
	print 'incorrect format : -s YYYY-MM-DD  -e YYYY-MM-DD -d [month/day/hours]'


def main(argv):	          
	try:                                
		opts, args = getopt.getopt(argv, "hs:e:d:", ["help", "start=","end=","deltatime="])
	except getopt.GetoptError:          
		usage()                         
		sys.exit(2)                     
	                	
	for opt, arg in opts:         
		if opt in ("-s", "--start"): 
			
			start = datetime.strptime(arg,'%Y-%m-%d:%H-%M-%S') 

			start = int(start.strftime("%s"))

		elif opt in ("-e", "--end"): 
			end = datetime.strptime(arg,'%Y-%m-%d:%H-%M-%S')
			end= int(end.strftime("%s"))
		
		elif opt in ("-d", "--deltatime"): 
			deltatime = arg 
			print arg
		
		else:
			print 'inconnu'
		
	try:
		
		obj= Db_slurm_event()
		output = open('event_fevrier.txt','r')
		print obj.extract_event(start,end,deltatime,output)		
			
	except:
		usage()
		sys.exit(2)
		
		
if __name__ == "__main__":
	main(sys.argv[1:])
1351728000,1354319999
