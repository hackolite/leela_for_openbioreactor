from datetime import *
import time
import sys 

"""liste debut fin de mois"""
""" liste debut fin de jours""" 

file_read = open('job.txt','r')
liste_read = file_read.readlines()


#mysql header
header = ["job_db_inx" , "mod_time", "deleted", "account", "cpus_req", "cpus_alloc", "derived_ec", "derived_es", "exit_code", "job_name", "id_assoc", "id_block", "id_job", "id_qos", "id_resv", "id_wckey" \
, "id_user", "id_group", "kill_requid", "nodelist", "nodes_alloc", "node_inx", "partition", "priority", "state", "timelimit", "time_submit", "time_eligible", "time_start", "time_end"\
, "time_suspended", "wckey", "track_steps"]



#job_state
state = ['JOB_PENDING','JOB_RUNNING','JOB_SUSPENDED','JOB_COMPLETE','JOB_CANCELLED','JOB_FAILED','JOB_TIMEOUT','JOB_NODE_FAIL','JOB_END']




def get_job_attr(key,list_read):
        num = header.index(key)
        return header[int(num)], list_read.split(', ')[int(num)]
count = 0

def intervalle(dt,end,step):	
	result = []
	while dt < end:
		#result.append(dt.strftime('%Y-%m-%d %H:%M:%S'))
		dtf = dt+step
		result.append((dt,dtf))
		dt += step
	return result



dt = datetime(2012, 02, 01)
end = datetime(2012, 02, 29, 23, 59, 59)
step = timedelta(days=1)
tuple_intervalle= intervalle(dt,end,step)
out_list = []
tmp_filtre = []


for i in liste_read:
	result = get_job_attr('time_start',i)[1].replace('L','') 
	result = datetime.fromtimestamp(float(result))
	if dt < result < end:
		tmp_filtre.append(i)
	
	
for j in tuple_intervalle:	
	start=j[0]
	end=j[1]
	for i in tmp_filtre: 
		resultat = get_job_attr('time_start',i)[1].replace('L','') 
		resultat = datetime.fromtimestamp(float(resultat))  
		if start < resultat < end:
			count = count+1	
	out_list.append(count)
	count=0 	
print out_list
