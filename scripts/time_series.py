import datetime



def intervalle(dt,end,step):	
	result = []
	while dt < end:
		#result.append(dt.strftime('%Y-%m-%d %H:%M:%S'))
		dtf = dt+step
		result.append((dt.strftime("%s"),dtf.strftime("%s")))
		dt += step
	return result	
	

if __name__ == "__main__":
	
	dt = datetime.datetime(2010, 12, 01)
	end = datetime.datetime(2010, 12, 30, 23, 59, 59)
	step = datetime.timedelta(days=5)
	intervalle(dt,end,step)
