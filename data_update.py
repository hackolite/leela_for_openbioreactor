import random
import os 
from tempfile import mkstemp
from shutil import move
from os import remove, close



#_script de generation de lobjet_data
#_le protocole utilise est le wifi par multipoint.

#donnees recues par arduino_serial



import serial

# extraction des donnees temperature et luminosite

	
class BIOR_OBJ():
       
    def __init__(self, id_bioreactor):
		""" check bioreactor id check connection """
		self.bior_id = 'COM15'
		self.connect_speed = 9600
    
    
    def __build_dict__(self):
		pass
   
    
    def get_temp(self):		
		ser = serial.Serial(self.bior_id, self.connect_speed)
		return ser.readline().split()[0]





def replace(file_path, letter, appender):
    
    subst = letter + ' ='
    mylist = []
    #Create temp file
    fh, abs_path = mkstemp()
    new_file = open(abs_path,'w')
    old_file = open(file_path)
    for line in old_file:
		if line.count(subst)>0:
			
			alist = line.split('=')[1]
			alist = alist.replace(' ','')
			new_alist = alist.split(',')
			del new_alist[0]
			new_alist.append(appender)
			
			mylist = ((str(new_alist).strip('[]')))
			mylist = mylist.replace("'","")
			mylist = mylist.replace("n","")
			mylist = mylist.replace("\\","")
			mylist = mylist.replace(" ","")
			subst_test = subst + ' '+ mylist + ('\n')
			
			new_file.write(line.replace(line, subst_test))
		
		
		
		else :
			new_file.write(line)
    #close temp file
    new_file.close()
    close(fh)
    old_file.close()
    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)

i = 0
while 1==1 :
	
	if i < 50 :
		i = i+1
		pass
	
	else:
		
		value = BIOR_OBJ('COM15')
		print value.get_temp()
		replace('tmp/data.csv', 'a', value.get_temp())
		i = 0
