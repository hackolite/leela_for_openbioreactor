import textwrap
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from tornado.options import define, options
from Sax import Sax
from Graph_Class import Graph_Class
from Template import Template 
import os 
from optparse import OptionParser
import cStringIO
from Sax import *
import time

########################################################################################################
#lance l'application     #
########################################################################################################


parser = OptionParser()

class GraphHandler(tornado.web.RequestHandler):        
    
    def get(self, input):
        
        obj = Sax.Return_XML()
        obj.enumerate_page()
        HTML_page = Template.Graph_Javascript()
        self.write(HTML_page.get_HTML(input))
        #print HTML_page.get_HTML(input)
        #print HTML_page
        
        
class HomeHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index_prot.html")

class Pdf_Creator():
    def __init__(self):
        self.init = 1
        self.output_path = 'out.pdf'

    
    def HTML_doclist(self) :
        list_html =  os.listdir('.') 
        list_html.sort()
        return list_html 

	
    def pdf_creation(self,string):
        cmd = os.popen("./htmltopdf " + string + " "+ self.output_path)
        cmd.close()
	

class main_CLI():

    def index_html(self,TITLE,COMMENTAIRE,DATE):
        index= open('0_index.html','w')
    
        string ="""<html>
	<head>
		<title></title>
	</head>
	<body>
		<p style="text-align: center;">
			&nbsp;</p>
		<p style="text-align: center;">
			&nbsp;</p>
		<p style="text-align: center;">
			&nbsp;</p>
		<p style="text-align: center;">
			&nbsp;</p>
		<p style="text-align: center;">
			&nbsp;</p>
		<p style="text-align: center;">
			&nbsp;</p>
		<p style="text-align: center;">
			&nbsp;</p>
		<p style="text-align: center;">
			&nbsp;</p>
		<p style="text-align: center;">
			&nbsp;</p>
		<p style="text-align: center;">
			<span style="font-size: 72px;">%s</span></p>
		<p style="text-align: center;">%s</p>
		<p style="text-align: center;">%s</p>
	</body>
    </html>""" % (TITLE,COMMENTAIRE,DATE)
        index.write(string)
        index.close()
    
    def create_HTML(self,count,input):
        HTML_page = Template.Graph_Javascript()
        myfile_tmp = open(str(count)+'_'+str(input)+'.html','w')
        myfile_tmp.write(HTML_page.get_HTML(input))
        myfile_tmp.close()
    
  
    def main(self):        
        
        parser = OptionParser()
        parser.add_option("-x", "--XML", dest="filenamexml",help="xml for website building", metavar="FILE")
        parser.add_option("-p", "--port", dest="port",help="port for website", metavar="FILE")
        parser.add_option("-d", "--table", dest="table",help="write csv datas description", metavar="FILE")
        parser.add_option("-o", "--report", dest="filereport",help="write report to FILE", metavar="FILE")
        (options, args) = parser.parse_args()
            
        
        if options.filereport :
            self.index_html('RAPPORT STATISTIQUE',options.filereport,time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime()))
            string_file=""
            obj_pdf = Pdf_Creator()
            #
            
            for file in obj_pdf.HTML_doclist():
                if file.count(".html")>0:
                    string_file =  string_file +' '+ file 
            obj_pdf.pdf_creation(string_file)
            
            
            new_count = 1
            obji = Sax.Return_XML()
            for page in obji.enumerate_page():
            
                obj.create_HTML(new_count, page)
                new_count = new_count + 1 
        
        else:            
            define("port", default=8888, help="run on the given port", type=int)
            options = tornado.options.parse_command_line()
            settings = {"static_path": os.path.join(os.path.dirname(__file__), "static")}
            app = tornado.web.Application(handlers=[(r"/(\w+)", GraphHandler),(r'/static/(.*)', tornado.web.StaticFileHandler, {'path': "static"})])
            http_server = tornado.httpserver.HTTPServer(app)
            http_server.listen(8888)
            tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    obj = main_CLI()
    obj.main()
