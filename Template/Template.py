import os 
from Sax import Sax
from string import Template
import Calculation
from Calculation import formule_list


class Graph_Javascript():
       
    global skel
    
    def __init__(self):  
        ''' dictionnary datas   '''
        """this class build the javascript webpages"""  
        
        self.dictionnary = [{'label': '"IVANOE"', 'data' :[[1, 794], [2, 99060],[3, 483794], [4, 9060]]},{'label': '"ZUMBROTA"', 'data' :[[1, 83794], [2, 9060],[3, 483794], [4, 79060]]},{'label': '"BGQ"', 'data' :[[1, 483794], [2, 479060],[3, 483794], [4, 479060]]},{'label': '"ASTER"', 'data' :[[1, 82900], [2, 479040],[3, 442900], [4, 379040]]}]
        self.dataset_name = 'dataset'
        self.FIG_NAME = 'FIG_NAME'
        self.title = 'title'
        self.COMMENT = 'COMMENT'
        self.links =  ['link']
        self.choices= 'choice'
        """ build the dataset tool"""
    
    def dataset_name_funct(self):
        str_DATA = ''
        for dicti in self.dictionnary:
            str_DATA = str_DATA + """  %s : {
            label: %s,
            data: %s}
        ,""" % (dicti['label'],dicti['label'],dicti['data'])
            DATA_str = """var """+ self.dataset_name + """ = {""" +  str_DATA +"};"
        return DATA_str
        
    """ we cant use template, dollars is interprated by javascript"""
    
    def script_var(self):
 	
        PLOT_DECLARE = str("""
<script type="text/javascript">        
$(function () {
                        
"""+ self.dataset_name_funct() + """

    // hard-code color indices to prevent them from shifting as
    // countries are turned on/off
    var i = 0;
    $.each("""+self.dataset_name+""", function(key, val) {
        val.color = i;
        ++i;
    });
    
    // insert checkboxes 
    var choiceContainer = $("#"""+ self.choices + """");
    $.each("""+self.dataset_name+""", function(key, val) {
        choiceContainer.append('<br/><input type="checkbox" name="' + key +
                               '" checked="checked" id="id' + key + '">' +
                               '<label for="id' + key + '">'
                                + val.label + '</label>');
    });
    choiceContainer.find("input").click(plotAccordingToChoices);

    
    
    function plotAccordingToChoices() {
        var data = [];

        choiceContainer.find("input:checked").each(function () {
            var key = $(this).attr("name");
            if (key && """+self.dataset_name+"""[key])
                data.push("""+self.dataset_name+"""[key]);
        });

        if (data.length > 0)
            $.plot($("#"""+self.FIG_NAME+""""), data, {
                yaxis: { min: 0 },
                xaxis: { tickDecimals: 0 }
            });
    }

    plotAccordingToChoices();
    
    
     var time = new Date().getTime();
     $(document.body).bind("mousemove keypress", function(e) {
         time = new Date().getTime();
     });

     function refresh() {
         if(new Date().getTime() - time >= 60000) 
             window.location.reload(true);
         else 
             setTimeout(refresh, 10000);
     }

     setTimeout(refresh, 1000);
});
</script>
""")
        return PLOT_DECLARE


 
		
    #function to modify graph size in my template
    def image_var(self):
        my_string = '<div id='+ self.FIG_NAME + ' style="width:1500px;height:200px;"></div>'	
        return my_string

    #function to modify title in the template
    def title_var(self):
		return "<h1>"+self.title+"</h1>"

    #function to modify comment at the bottom of the graph
    def comment_var(self):
		var = self.COMMENT
		return var
		
    #function to parse list string from XML and retrun it 
    def get_links(self):
		
		str_links = ''
		self.links = self.links.replace('[','')
		self.links = self.links.replace(']','')
		
		for lin in self.links.split(','):
			lin = "<a href=http://localhost:8888/%s>%s</a>" % (lin,lin) 
			str_links = str_links + lin + '\n'
		return str_links

    #return the choice list
    def get_choices(self):
        return '<p id="'+self.choices+'">Show:</p>'

    #after the calculation, return the data dictionnary
    def get_dictionnary(self):
		liste_dico = [] 
		
		o = formule_list.Formulist()
		o_result = o.read_csv('./tmp/data.csv')
		
		
		for i in range(len(self.DATA_NAME.split(','))):
			count = 0
			dictionnaire = {}
			list_calc = []
			cal = str(self.DATA_FUNC[i])
			list_tmp = o.calculation(o_result,cal)
			for y in list_tmp:
				list_calc.append([count,y])
				count = count+1
			dictionnaire['label']='"'+self.DATA_NAME.split(',')[i]+'"'
			dictionnaire['data'] = list_calc
			liste_dico.append(dictionnaire)
		return liste_dico
		
    
    skel = Template("""<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<META http-equiv="Refresh" content="5">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>EXAMPLE</title>
<link href="layout.css" rel="stylesheet" type="text/css">
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="../excanvas.min.js"></script><![endif]-->
<script language="javascript" type="text/javascript" src="./static/jquery.js"></script>
<script language="javascript" type="text/javascript" src="./static/jquery.flot.js"></script>
</head>
</body>
</html>    
<body>
$GRAPH
</body>
</html>""")    
    
    
    def get_HTML(self,PAGE_NAME):
        result_graph = ''
        obj = Sax.Return_XML()
        obj.get_datas(PAGE_NAME)
        LISTE_graphXML = obj.get_datas(PAGE_NAME)
        graph = Graph_Javascript()
        graph_TWO = Graph_Javascript()
    
        for graphXML in LISTE_graphXML: 
            graph = Graph_Javascript() 
            graph.dataset_name = graphXML.DATASET_NAME
            graph.FIG_NAME = graphXML.GRAPH_NAME
            graph.title = graphXML.TITLE
            graph.COMMENT = graphXML.COMMENT
            u_liste=[]
            graph.DATA_NAME = graphXML.DATA_NAM
            print graphXML.DATA_FUNC
            for u in graphXML.DATA_FUNC.split(','):               
				u_liste.append(str(u))
            graph.DATA_FUNC =  u_liste
            graph.links =  graphXML.LINKS
            graph.choices=  graphXML.GRAPH_NAME+'_CHOICE'               
            graph.dictionnary = graph.get_dictionnary()
            graph_str = str(graph.title_var()+graph.image_var()+graph.comment_var()+graph.get_choices()+graph.script_var()+graph.get_links())
            result_graph = result_graph+graph_str
    
        print result_graph  
        res = skel.substitute(GRAPH=result_graph)
        return res


if __name__ == "__main__":
	
    lajava = Graph_Javascript()
    lajava.get_HTML("CLUSTER")
